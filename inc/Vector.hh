#pragma once

#include "Size.hh"
#include <iostream>

using namespace std;

/*
 *  This class represents vector, it includes board and methods operating with this class.
 */
class Vector
{
  /*
   * Vector's body.
   */
  double _Tab[SIZE];

public:

  /*
   * Method gives acces to private Vector's elements.
   */
  double   operator [](int Ind) const {return _Tab[Ind];}
  
  /*
   * Method allows to write value to Vector.
   */
  double&  operator [](int Ind) {return _Tab[Ind];}

  /*
   * Method counts inner product of given object Vector's class and first vector.
   */
  double   operator & (const Vector &vec2) const;

  /*
   * Method adds given object Vector's class to first vector.
   */
  Vector  operator + (const Vector &vec2)const;

  /*
   * Method substracts given object Vector's class from first vector.
   */
  Vector  operator - (const Vector &vec)const;

  /*
   * Method multiplies given double by first vector.
   */
  Vector  operator * (const double  &component)const;

  /*
   * Method divides object Vector's class by given double.
   */
  Vector  operator / (const double  &factor)const;

  /*
   * Method compare two Vectors.
   */
  int operator == (const Vector &vec)const;
};

/*
 * Function enters data to object which is Vector's class using operator's overload.
 */
istream &operator>>(istream &stream, Vector &vec);

/*
 * Function display object which is Vector's class using operator's overload.
 */
ostream &operator<<(ostream &stream, const Vector &vec);