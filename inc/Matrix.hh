#pragma once

#include "Size.hh"
#include "Vector.hh"
#include <iostream>
using namespace std;

/*
 *  This class represents matrix, it includes board filled with vectors and methods operating with this class.
 */
class Matrix
{
  /*
   * Matrix's body.
   */
  Vector _Tab[SIZE];

  public:

 /*
  *  Method which gives acces to Matrix's rows.
  */
 Vector   operator [](int element)const{return this->_Tab[element];}

  /*
   * Method which allows to write in vector to Matrix's row.
   */
 Vector&  operator [](int element)     {return _Tab[element];}

 /*
  *  Method which allows to multiply Matrix's row by another vector.
  */
 Vector operator * (const Vector vec)const;
 
  /*
   * Method brings Matrix to Matrix filled with 0 under diagonal then counts Determinant.
   */
  double Determinant()const;

  /*
   * Method checks if in Matrix are some rows or columns filled with 0.
   */
  int Zero()const;

  /*
   * Method counts Determinant of Matrix
   */
  double Det();

  /*
   * Method transposes Matrix.
   */
  Matrix Transposition()const;

  /*
   * Method checks if in Matrix are same rows or columns.
   */
  int CheckSameColOrRow()const;

  /*
   * Method which allows to multiply Matrix by another Matrix.
   */
  Matrix operator *(const Matrix &mat)const;
};
  

/*
 * Function enters data to object which is Matrix's class using operator's overload.
 */
istream & operator >> (istream &StreamIn, Matrix &matrix);

/*
 * Function display object which is Matrix's class using operator's overload.
 */
ostream & operator << (ostream &StreamOut, const Matrix &matrix);