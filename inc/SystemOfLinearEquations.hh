#pragma once

#include <iostream>
#include <cmath>
#include "Vector.hh"
#include "Matrix.hh"

/*
 *  Tutaj trzeba opisac klase. Jakie pojecie modeluje ta klasa
 *  i jakie ma glowne cechy.
 */
class SystemOfLinearEquations
{
  /*
   * Origin transposed Matrix.
   */
  Matrix TransposedOriginMat;

  /*
   * Free words Vector.
   */
  Vector FreeWordsVector;

  /*
   * Results Vector.
   */
  Vector ResultsVector;

  /*
   * Mistake Vector.
   */
  Vector MistakeVector;

  /*
   * Length of Mistake Vector.
   */
  double MistakeVectorLength;

  /*
   * Information about results.
   */
  int Results;

public:
  /*
   * Method returns Origin Matrix.
   */
 Matrix   RetMat()const{return TransposedOriginMat;}

  /*
   * Method allows to write Origin Matrix.
   */
 Matrix&  WriteTransposedOriginMat()     {return TransposedOriginMat;}

 /*
  * Method returns Free Words Vector.
  */
 Vector   RetFreeWordsVector()const{return FreeWordsVector;}

  /*
   * Method allows to write Free Words Vector.
   */
 Vector&  WriteFreeWordsVector()     {return FreeWordsVector;}

 /*
  * Method returns Results Vector.
  */
 Vector   RetResultsVector()const{return ResultsVector;}

  /*
   * Method allows to write Results Vector.
   */
 Vector&  WriteResultsVector()     {return ResultsVector;}

  /*
   * Method returns Mistake Vector.
   */
 Vector   RetMistakeVector()const{return MistakeVector;}

  /*
   * Method allows to write Mistake Vector.
   */
 Vector&  WriteMistakeVector()     {return MistakeVector;}

  /*
   * Method returns length of Mistake Vector.
   */
 double RetMistakeVectorLength()const{return MistakeVectorLength;}

  /*
   * Method allows to write Mistake Vector's length.
   */
 double& WriteMistakeVectorLength(){return MistakeVectorLength;}

  /*
   * Method uses Cramer's Method to count Results Vector.
   */
 Vector Cramer();

  /*
   * Metohod returns information about results.
   */
 int RetResults()const{return Results;}

  /*
   * Method allows to write information about results.
   */
 int& WriteResults()  {return Results;}

  /*
   * Method counts Mistake Vector.
   */
 Vector CountMistakeVector() const{Vector result; return result = ((this->RetMat().Transposition()*ResultsVector)-FreeWordsVector);}

  /*
   * Method counts length of Mistake Vector.
   */
 double LengthMistakeVector()const{double length; return length = sqrt(MistakeVector&MistakeVector);}
};

/*
 * Function enters data to SystemOfLinearEquations' class using operator's overload.
 */
istream &operator>>(istream &StreamIn, SystemOfLinearEquations &system);

/*
 * Function display SystemOfLinearEquations' class using operator's overload.
 */
ostream &operator<<(ostream &stream, const SystemOfLinearEquations &system);