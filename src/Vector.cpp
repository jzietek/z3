#include "Vector.hh"
using namespace std;

/*
 * Function enters data to object which is Vector's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Vector's class.
 * Returns:
 *   Function returns reference to input stream.
 */
std::istream &operator>>(std::istream &StreamIn, Vector &vec){
    for (int i = 0; i < SIZE; i++){
        StreamIn >> vec[i];
    }
    return StreamIn;
}


/*
 * Function display object which is Vector's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Vector's class.
 * Returns:
 *   Function returns reference to output stream.
 */
std::ostream &operator<<(std::ostream &StreamOut, const Vector &vec){
  for (int i = 0; i < SIZE; i++)
  {
      StreamOut << "\t" << vec[i];
      if (i == (SIZE - 1))
      {
          StreamOut << endl;
      }
  }
  return StreamOut;
}

/*
 * Method adds given object Vector's class to first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
Vector Vector::operator+ (const Vector &vec) const{
    Vector result;
    for (int i = 0; i < SIZE; i++)
    {
        result[i]=this->_Tab[i]+vec[i];
    }
    return result;
}

/*
 * Method substracts given object Vector's class from first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
Vector Vector::operator - (const Vector &vec)const{
    Vector result;
    for (int i = 0; i < SIZE; i++)
    {
        result[i]=this->_Tab[i]-vec[i];
    }
    return result;
}


/*
 * Method counts inner product of given object Vector's class and first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
double Vector::operator & (const Vector &vec2) const
{
  double result = 0;
  int Ind = -1;
  for(double WspVectora:_Tab) result +=WspVectora*vec2[++Ind];
  return result;
}

/*
 * Method multiplies given float by first vector.
 * Parameters:
 *  component - reference to given float (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
Vector  Vector::operator * (const double &component)const{
    Vector result;
    for (int i = 0; i < SIZE; i++)
    {
        result[i]=this->_Tab[i]*component;
    }
    return result;
}

/*
 * Method divides object Vector's class by given float.
 * Parameters:
 *   factor - reference to double.
 * Returns:
 *   Function returns result of operation.
 */
Vector Vector::operator / (const double  &factor)const{
    Vector result;
    for (int i = 0; i < SIZE; i++)
    {
        result[i]=this->_Tab[i]/factor;
    }
    return result;
}

/*
 * Method compare two Vectors
 * Parameters:
 *   vec - reference to ovject Vector's class.
 * Returns:
 *   Function returns information: 1 - Vectors are the same, 0 - Vectors aren't the same.
 */
int Vector::operator==(const Vector &vec)const{
    int counter = 0;
    for (int i = 0; i < SIZE; i++)
    {
        if (this->_Tab[i]==vec._Tab[i])
        {
        ++counter;
        }
        
        if (counter == SIZE)
        {
            return 1;
        }  
    }
    return 0;
}