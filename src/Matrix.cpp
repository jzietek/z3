#include "Matrix.hh"

using namespace std;

/*
 * Function enters data to object which is Matrix's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Matrix's class.
 * Returns:
 *   Function returns reference to input stream.
 */
istream &operator >> (istream &StreamIn, Matrix &matrix){
    double value;
    for (int i = 0; i < SIZE; i++){
        for (int j = 0; j < SIZE; j++)
        {
            StreamIn >> value;
            matrix[i][j]=value;
        }
    }
    return StreamIn;
}

/*
 * Function display object which is Matrix's class using operator's overload.
 * Parameters:
 *   StreamOut - reference to output stream,
 *   matrix - reference to given object Matrix's class.
 * Returns:
 *   Function returns reference to output stream.
 */
ostream &operator<<(ostream &StreamOut, const Matrix &matrix){
    int j;
    for (int i = 0; i < SIZE; i++){
        for (j = 0; j < SIZE; j++)
        {
        StreamOut << "\t" << matrix[i][j];
         if (j == SIZE - 1)
        {
            cout << endl;
        }}}
    return StreamOut;
}

/*
 * Method checks if in Matrix are some rows or columns filled with 0.
 * Returns:
 *   Information: 1 - there isn't any row or column filled with 0, 0 - there is a row or column filled with 0.
 */
int Matrix::Zero()const{
    int counter;
    for (int i = 0; i < SIZE; i++){
        counter = 0;
        for (int j = 0; j < SIZE; j++)
        {
            if (this->_Tab[i][j]==0)
            {
                ++counter;
            }
            if (counter == SIZE)
            {
                return 0;
            }
            
        }
    }

    for (int i = 0; i < SIZE; i++){
        counter = 0;
        for (int j = 0; j < SIZE; j++)
        {
            if (this->_Tab[j][i]==0)
            {
                ++counter;
            }
            if (counter == SIZE)
            {
                return 0;
            }
            
        }
    }
    return 1;
}

/*
 * Method checks if in Matrix are same rows or columns.
 * Returns:
 *   Method returns information: 1 - there are same rows or columns, 0 - there aren't any same rows or columns.
 */
int Matrix::CheckSameColOrRow()const{
    int colorrow;
    Vector vec1, vec2;
    int counter = 0;
    double multiplier;
    Matrix mat;
    mat=*this;

    for (int i = 0; i < SIZE; i++){
        vec1=mat[i];
        multiplier=vec1[i];
        for (int j = i + 1; j < SIZE; j++)
        {
            vec2=mat[j];
            if (vec2[i!=0])
            {
                multiplier/=vec2[i];
            }else
            {
                multiplier/=1;
            }
            if (vec1==vec2*multiplier)
            {
                counter++;
            }  
        }                
    }
    if (counter>=1)
    {
        return colorrow = 1;
    }

    mat=mat.Transposition();

for (int i = 0; i < SIZE; i++){
        vec1=mat[i];
        multiplier=vec1[i];
        for (int j = i + 1; j < SIZE; j++)
        {
            vec2=mat[j];
            if (vec2[i!=0])
            {
                multiplier/=vec2[i];
            }else
            {
                multiplier/=1;
            }
            if (vec1==vec2*multiplier)
            {
                counter++;
            }  
        }                
    }
    if (counter>=1)
    {
        return colorrow = 1;
    }

    return colorrow = 0;
}

/*
 * Method counts Determinant of Matrix.
 * Returns:
 *   Function returns value of counted Determinant.
 */
double Matrix::Det(){
    double result = 1;
    for (int i = 0; i < SIZE; i++)
    {
        result*=this->_Tab[i][i];
    }
    return result;
}

/*
 * Method brings Matrix to Matrix filled with 0 under diagonal then counts Determinant.
 * Returns:
 *   Function returns value of counted Determinant.
 */
double Matrix::Determinant()const{
    Matrix mat;
    mat=*this;
    int i,j,k;
    Vector row;
    int minus = 0;
    double multiplier, det = 0;
    int zero;
    int colorrow;

    zero=this->Zero();

    if (zero==0)
    {
        return det;
    }

    colorrow=this->CheckSameColOrRow();

    if (colorrow == 1)
    {
        return det;
    }
    
    
    for(i = 0; i < SIZE - 1; i++){
        if (mat[i][i]==0)
        {
            for (int r = i + 1; r < SIZE; r++)
            {
                if (mat[r][i] != 0)
                {
                    row=mat[i];
                    mat[i]=mat[r];
                    mat[r]=row;
                    ++minus;
                    r=SIZE;
                }
            }
        }
      
        for(j = i + 1; j < SIZE; j++){
            multiplier = -mat[j][i] / mat[i][i];
                for(k = i; k < SIZE; k++){
                    mat[j][k] += multiplier * mat[i][k];
    }}}

    det = mat.Det();
    
        if (minus%2!=0)
        {
            det=-det;
        }

    return det;
}

/*
 * Method transposes Matrix.
 * Returns:
 *   Function returns transposed Matrix.
 */
Matrix Matrix::Transposition()const{
    Matrix mat;
    Vector vec;
    mat = *this;
    for (int i = 0; i < SIZE; i++){
        for (int j = 0; j < SIZE; j++){
            vec[j]=this->_Tab[j][i];
        }
        
        mat[i]=vec;
    }
    return mat;
}

/*
 * Method which allows to multiply Matrix's row by another vector.
 * Returns:
 *   Function returns result of the equation.
 */
Vector Matrix::operator * (const Vector vec)const{
    Vector result;
    for (int i = 0; i <SIZE; i++)
    {
        result[i]=this->_Tab[i]&vec;
    }
    return result;
}

/*
 * Method which allows to multiply Matrix by another Matrix.
 * Parameters:
 *   &mat - reference to the second factor of multiplication.
 * Returns:
 *   Function returns result of the equation.
 */
Matrix Matrix::operator *(const Matrix &mat)const{
    Matrix result, transpositionmat;
    transpositionmat=mat.Transposition();

    for (int i = 0; i < SIZE; i++){
        for (int j = 0; j < SIZE; j++){
            result[i][j]=this->_Tab[i]&transpositionmat[j];
        }   
    }
    
    return result;
}