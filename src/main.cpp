#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;


int main()
{
       SystemOfLinearEquations system;
       cin >> system;
       system.WriteResultsVector()=system.Cramer();
       system.WriteMistakeVector()=system.CountMistakeVector();
       system.WriteMistakeVectorLength()=system.LengthMistakeVector();
       cout << system << endl;
       cout << "Test of modification (data is entered from file equation.dat):" << endl << endl;
       Matrix A, B, result;
       cin >> A;
       cin >> B;
       result=A*B;
       cout << result << endl;
       cout << "The end of the test." << endl;
       }