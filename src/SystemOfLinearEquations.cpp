#include "SystemOfLinearEquations.hh"


/*
 * Method uses Cramer's Method to count Results Vector.
 * Returns:
 *   Function returns Results Vector.
 */
Vector SystemOfLinearEquations::Cramer(){
    Vector result;
    Matrix mat;
    double det;
    int counter = 0;

    for (int i = 0; i < SIZE; i++)
    {
        result[i]=0;
    }
    

    mat=this->RetMat();
    det=mat.Determinant();

    if (det==0)
    {
        ++counter;
    }
    
        for (int j = 0; j < SIZE; j++){
            mat=this->RetMat();
            mat[j]=this->RetFreeWordsVector();
            result[j]=mat.Determinant();
            if (result[j]==0)
            {
                counter++;
            }else
            {
                result[j]/=det;
            }
        }

        if (det==0)
            {
                if (counter == SIZE + 1)
                {
                    this->WriteResults() = 2;
                    return result;
                }else
                {
                    this->WriteResults() = 0;
                    return result;
                }   
            }

    this->WriteResults() = 1;
    return result;
}

/*
 * Function enters data to object which is SystemOfLinearEquations' class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   System - reference to given object SystemOfLinearEquations' class.
 * Returns:
 *   Function returns reference to input stream.
 */
istream &operator>>(istream &StreamIn, SystemOfLinearEquations &System){
    Matrix mat;
    Vector vec;
    StreamIn >> mat;
    StreamIn >> vec;
    System.WriteTransposedOriginMat()=mat;
    System.WriteFreeWordsVector()=vec;
    return StreamIn;
}

/*
 * Function display object which is SystemOfLinearEquations' class using operator's overload.
 * Parameters:
 *   StreamOut - reference to output stream,
 *   System - reference to given object SystemOfLinearEquations' class.
 * Returns:
 *   Function returns reference to output stream.
 */
ostream &operator<<(ostream &StreamOut, const SystemOfLinearEquations &System){
    StreamOut << "Matrix A^T:"                                                 << endl << endl;
    StreamOut << System.RetMat()                                               << endl << endl;
    StreamOut << "Free words vector b:"                                        << endl << endl;
    StreamOut << System.RetFreeWordsVector()                                   << endl << endl;
    if (System.RetResults()==1){
        StreamOut << "Result x = (";
        for (int i = 1; i <= SIZE; i++)
        {
            cout << "x_" << i;
            if (i!=SIZE)
            {
                StreamOut << ", ";
            }}; 
        StreamOut << ")"                                                               << endl;
        StreamOut << System.RetResultsVector()                                 << endl << endl;
        StreamOut << "Mistake vector:\tAx-b :" << endl << System.RetMistakeVector()    << endl;
        StreamOut << "His length:  ||Ax-b|| = " << System.RetMistakeVectorLength()     << endl;
    }
    if (System.RetResults()==0)
    {
        StreamOut << "There are no results." << endl;
    }
    if (System.RetResults()==2)
    {
        StreamOut << "There are no results or there is infinity of results." << endl;
    }
    
    
    return StreamOut;
}